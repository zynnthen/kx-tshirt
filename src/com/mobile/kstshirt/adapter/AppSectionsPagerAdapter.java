package com.mobile.kstshirt.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mobile.kstshirt.fragment.Back;
import com.mobile.kstshirt.fragment.Front;
import com.mobile.kstshirt.fragment.Left;
import com.mobile.kstshirt.fragment.Right;

public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

	static Fragment fragment = null;
	public AppSectionsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
		case 0:
			fragment = Front.getInstance();
			break;
		case 1:
			fragment = new Back();
			break;
		case 2:
			fragment = new Left();
			break;
		case 3:
			fragment = new Right();
			break;
		default:
			break;
		}
		return fragment;
	}

	@Override
	public int getCount() {
		return 4;
	}

	public Fragment getFragment(int i)
	{
		Fragment fragment = null;
		switch (i) {
		case 0:
			fragment = Front.getInstance();
			break;
		case 1:
			fragment = new Back();
			break;
		case 2:
			fragment = new Left();
			break;
		case 3:
			fragment = new Right();
			break;
		default:
			break;
		}
		return fragment;
	}
}
