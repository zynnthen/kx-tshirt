package com.mobile.kstshirt.colorpicker;

public class TshirtColorOld {
	
	public static int getHue(int shirtCode) {
		int hue = 0;

		switch (shirtCode) {
		// white
		case 0:
			hue = 0;
			break;
		// black
		case 1:
			hue = 0;
			break;
		// red
		case 2:
			hue = 0;
			break;
		// green
		case 3:
			hue = 68;
			break;
		// blue
		case 4:
			hue = 162;
			break;
		// purple
		case 5:
			hue = 190;
			break;
		}

		return hue;
	}
	
	public static int getSat(int shirtCode) {
		int sat = 0;

		switch (shirtCode) {
		// white
		case 0:
			sat = 0;
			break;
		// black
		case 1:
			sat = -256;
			break;
		// red
		case 2:
			sat = 231;
			break;
		// green
		case 3:
			sat = 230;
			break;
		// blue
		case 4:
			sat = 174;
		// purple
		case 5:
			sat = 152;
			break;
		}

		return sat;
	}
	
	public static int getVal(int shirtCode) {
		int val = 0;

		switch (shirtCode) {
		// white
		case 0:
			val = 0;
			break;
		// black
		case 1:
			val = -174;
			break;
		// red
		case 2:
			val = 0;
			break;
		// green
		case 3:
			val = 0;
			break;
		// blue
		case 4:
			val = -68;
		// purple
		case 5:
			val = 0;
			break;
		}

		return val;
	}
	

}
