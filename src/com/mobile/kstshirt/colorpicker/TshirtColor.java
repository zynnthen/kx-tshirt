package com.mobile.kstshirt.colorpicker;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;

public class TshirtColor {
	
	static int shirtCode = 0;

	public class GroupBitmap {
		public Bitmap bitmapDest;
	};

	public static int getHue() {
		int hue = 0;

		switch (shirtCode) {
		// white
		case 0:
			hue = 0;
			break;
		// black
		case 1:
			hue = 0;
			break;
		// red
		case 2:
			hue = 0;
			break;
		// green
		case 3:
			hue = 68;
			break;
		// blue
		case 4:
			hue = 162;
			break;
		// purple
		case 5:
			hue = 190;
			break;
		}

		return hue;
	}

	public static int getSat() {
		int sat = 0;

		switch (shirtCode) {
		// white
		case 0:
			sat = 0;
			break;
		// black
		case 1:
			sat = -256;
			break;
		// red
		case 2:
			sat = 231;
			break;
		// green
		case 3:
			sat = 230;
			break;
		// blue
		case 4:
			sat = 174;
			// purple
		case 5:
			sat = 152;
			break;
		}

		return sat;
	}

	public static int getVal() {
		int val = 0;

		switch (shirtCode) {
		// white
		case 0:
			val = 0;
			break;
		// black
		case 1:
			val = -174;
			break;
		// red
		case 2:
			val = 0;
			break;
		// green
		case 3:
			val = 0;
			break;
		// blue
		case 4:
			val = -68;
			// purple
		case 5:
			val = 0;
			break;
		}

		return val;
	}

	// Convert Bitmap from Color to HSV, then HSV to Color

	public static GroupBitmap convertColorHSVColor(Bitmap src, GroupBitmap convertedGroupBitmap) {

		// GroupBitmap convertedGroupBitmap = new GroupBitmap();

		int w = src.getWidth();
		int h = src.getHeight();

		int[] mapSrcColor = new int[w * h];
		int[] mapDestColor = new int[w * h];

		int[] mapHue = new int[w * h];
		int[] mapSat = new int[w * h];
		int[] mapVal = new int[w * h];

		float[] pixelHSV = new float[3];

		src.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

		int index = 0;
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {

				// Convert from Color to HSV
				Color.colorToHSV(mapSrcColor[index], pixelHSV);

				/*
				 * Represent Hue, Saturation and Value in separated color of R,
				 * G, B.
				 */
				mapHue[index] = Color
						.rgb((int) (pixelHSV[0] * 255 / 360), 0, 0);
				mapSat[index] = Color.rgb(0, (int) (pixelHSV[1] * 255), 0);
				mapVal[index] = Color.rgb(0, 0, (int) (pixelHSV[2] * 255));

				// Convert back from HSV to Color
				mapDestColor[index] = Color.HSVToColor(pixelHSV);

				index++;
			}
		}

		Config destConfig = src.getConfig();
		/*
		 * If the bitmap's internal config is in one of the public formats,
		 * return that config, otherwise return null.
		 */

		if (destConfig == null) {
			destConfig = Config.RGB_565;
		}

		convertedGroupBitmap.bitmapDest = Bitmap.createBitmap(mapDestColor, w,
				h, destConfig);

		return convertedGroupBitmap;
	}

	// Update HSV according to selected color setting

	public static GroupBitmap updateHSV(Bitmap src,
			GroupBitmap convertedGroupBitmap) {

		int progressHue = getHue();
		int progressSat = getSat();
		int progressVal = getVal();

		float settingHue = (float) progressHue * 360 / 256;
		float settingSat = (float) progressSat / 256;
		float settingVal = (float) progressVal / 256;

		// GroupBitmap convertedGroupBitmap = new GroupBitmap();

		int w = src.getWidth();
		int h = src.getHeight();

		int[] mapSrcColor = new int[w * h];
		int[] mapDestColor = new int[w * h];

		int[] mapHue = new int[w * h];
		int[] mapSat = new int[w * h];
		int[] mapVal = new int[w * h];

		float[] pixelHSV = new float[3];

		src.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

		int index = 0;
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {

				// Convert from Color to HSV
				Color.colorToHSV(mapSrcColor[index], pixelHSV);

				// Adjust HSV
				pixelHSV[0] = pixelHSV[0] + settingHue;
				if (pixelHSV[0] < 0) {
					pixelHSV[0] = 0;
				} else if (pixelHSV[0] > 360) {
					pixelHSV[0] = 360;
				}

				pixelHSV[1] = pixelHSV[1] + settingSat;
				if (pixelHSV[1] < 0) {
					pixelHSV[1] = 0;
				} else if (pixelHSV[1] > 1) {
					pixelHSV[1] = 1;
				}

				pixelHSV[2] = pixelHSV[2] + settingVal;
				if (pixelHSV[2] < 0) {
					pixelHSV[2] = 0;
				} else if (pixelHSV[2] > 1) {
					pixelHSV[2] = 1;
				}

				/*
				 * Represent Hue, Saturation and Value in separated color of R,
				 * G, B.
				 */
				mapHue[index] = Color
						.rgb((int) (pixelHSV[0] * 255 / 360), 0, 0);
				mapSat[index] = Color.rgb(0, (int) (pixelHSV[1] * 255), 0);
				mapVal[index] = Color.rgb(0, 0, (int) (pixelHSV[2] * 255));

				// Convert back from HSV to Color
				mapDestColor[index] = Color.HSVToColor(pixelHSV);

				index++;
			}
		}

		Config destConfig = src.getConfig();
		/*
		 * If the bitmap's internal config is in one of the public formats,
		 * return that config, otherwise return null.
		 */

		if (destConfig == null) {
			destConfig = Config.RGB_565;
		}

		convertedGroupBitmap.bitmapDest = Bitmap.createBitmap(mapDestColor, w,
				h, destConfig);

		return convertedGroupBitmap;
	}

}
