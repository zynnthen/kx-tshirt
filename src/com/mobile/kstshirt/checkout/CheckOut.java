package com.mobile.kstshirt.checkout;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.kstshirt.R;

public class CheckOut extends Activity {

	private TextView qty, total, call;
	private EditText xs, s, m, l, xl;
	private Button order;

	private int qtyAmount = 0;
	private int totalAmount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_out);

		initializeVariables();
	}

	private void initializeVariables() {
		xs = (EditText) findViewById(R.id.xsEdit);
		s = (EditText) findViewById(R.id.sEdit);
		m = (EditText) findViewById(R.id.mEdit);
		l = (EditText) findViewById(R.id.lEdit);
		xl = (EditText) findViewById(R.id.xlEdit);

		xs.addTextChangedListener(txtChangedListener);
		s.addTextChangedListener(txtChangedListener);
		m.addTextChangedListener(txtChangedListener);
		l.addTextChangedListener(txtChangedListener);
		xl.addTextChangedListener(txtChangedListener);

		qty = (TextView) findViewById(R.id.quantity);
		total = (TextView) findViewById(R.id.total);

		order = (Button) findViewById(R.id.submitBtn);
		order.setOnClickListener(submitClickListener);

		call = (TextView) findViewById(R.id.call);
	}

	private TextWatcher txtChangedListener = new TextWatcher() {
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		public void afterTextChanged(Editable s) {
			updateQuantity();
			updateTotal();
		}
	};

	private void updateQuantity() {
		qtyAmount = 0;
		
		if (!xs.getText().toString().equals("") && !(xs == null)){
			qtyAmount = Integer.parseInt(xs.getText().toString());
		}
		if (!s.getText().toString().equals("") && !(s == null)){
			qtyAmount = qtyAmount + Integer.parseInt(s.getText().toString());
		}
		if (!m.getText().toString().equals("") && !(m == null)){
			qtyAmount = qtyAmount + Integer.parseInt(m.getText().toString());
		}
		if (!l.getText().toString().equals("") && !(l == null)){
			qtyAmount = qtyAmount + Integer.parseInt(l.getText().toString());
		}
		if (!xl.getText().toString().equals("") && !(xl == null)){
			qtyAmount = qtyAmount + Integer.parseInt(xl.getText().toString());
		}		

		qty.setText(qtyAmount + " pcs");
	}

	private void updateTotal() {
		int price = 0;
		if (qtyAmount <= 20) {
			if (qtyAmount <= 5) {
				price = 30;
			} else if (qtyAmount >= 6 && qtyAmount <= 10) {
				price = 28;
			} else if (qtyAmount >= 11 && qtyAmount <= 20) {
				price = 25;
			}
			totalAmount = qtyAmount * price;
			total.setText("RM " + totalAmount);
		} else {
			total.setText("N/A");
			order.setEnabled(false);
			order.setClickable(false);
			call.setText("Please call 1800-18-1800 for pricing");
		}
	}

	private OnClickListener submitClickListener = new OnClickListener() {
		public void onClick(View v) {
			Toast.makeText(getApplication(), "Your order have been submitted",
					Toast.LENGTH_SHORT).show();
		}
	};

}
