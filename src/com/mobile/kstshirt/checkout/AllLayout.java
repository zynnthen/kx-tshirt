package com.mobile.kstshirt.checkout;

import com.mobile.kstshirt.AppSingleton;
import com.mobile.kstshirt.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class AllLayout extends Activity {

	private ImageView front, back, left, right;
	private Button purchase;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_layout);

		initializeVariables();

		front.setImageBitmap(AppSingleton.getFront());
		back.setImageBitmap(AppSingleton.getBack());
		left.setImageBitmap(AppSingleton.getLeft());
		right.setImageBitmap(AppSingleton.getRight());
	}

	private void initializeVariables() {
		front = (ImageView) findViewById(R.id.imageViewFront);
		back = (ImageView) findViewById(R.id.imageViewBack);
		left = (ImageView) findViewById(R.id.imageViewLeft);
		right = (ImageView) findViewById(R.id.imageViewRight);

		purchase = (Button) findViewById(R.id.btnPurchase);
		purchase.setOnClickListener(purchaseClickListener);
	}

	private OnClickListener purchaseClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent (AllLayout.this, CheckOut.class);
			startActivity(i);
		}
	};
}
