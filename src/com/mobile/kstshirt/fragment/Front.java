package com.mobile.kstshirt.fragment;

import java.io.IOException;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.meg7.widget.SvgImageView;
import com.mobile.kstshirt.AppSingleton;
import com.mobile.kstshirt.R;
import com.mobile.kstshirt.adapter.RepeatListener;
import com.mobile.kstshirt.colorpicker.ColorPickerDialog;
import com.mobile.kstshirt.colorpicker.ColorPickerDialog.OnColorSelectedListener;
import com.mobile.kstshirt.drag.DragController;
import com.mobile.kstshirt.drag.DragLayer;

@SuppressLint({ "ResourceAsColor", "ClickableViewAccessibility" })
public class Front extends Fragment implements
		LoaderManager.LoaderCallbacks<Cursor>, View.OnLongClickListener,
		View.OnClickListener, View.OnTouchListener {

	public Context context;
	private SimpleCursorAdapter mAdapter;
	private View mContactFragmentView;

	private DragController mDragController;
	private DragLayer mDragLayer;
	private boolean mLongClickStartsDrag = true;
	private static Front myObject = null;

	static RelativeLayout fragmentLayout, masterView;
	static RelativeLayout viewSetting, textViewSetting, imgViewSetting;
	static SvgImageView imgView;
	ImageButton colorPick, add, minus, del, close;
	ImageButton colorPickTxt, addTxt, minusTxt, editTxt, delTxt, closeTxt;
	ImageButton delImg, closeImg;
	ImageButton txtBtn, imgBtn, circleBtn, squareBtn;
	View selectedItem = null;
	static Bitmap imgSource = null;

	private static int RESULT_LOAD_IMAGE = 1;

	static int shirtCode = 0;

	public static Front getInstance() {
		if (myObject == null)
			myObject = new Front();
		return myObject;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		mContactFragmentView = inflater.inflate(R.layout.fragment_rear,
				container, false);

		mDragController = new DragController(getActivity());
		initializeVariables();
		setupViews();
		
		int shirt = AppSingleton.getSelectedShirt();
		InputStream open = null;

		try {
			switch (shirt) {
			case 0:
				open = getActivity().getAssets().open(
						"tshirt_front_650.png");
				break;
			case 1:
				open = getActivity().getAssets().open(
						"vfront.jpg");
				break;
			case 2:
				open = getActivity().getAssets().open(
						"pfront.jpg");
				break;
			default:
				break;
			}
			imgSource = BitmapFactory.decodeStream(open);
		} catch (IOException e) {
			e.printStackTrace();
		}

		GroupBitmap groupBMResult = convertColorHSVColor(imgSource);
		imgView.setImageBitmap(groupBMResult.bitmapDest);
		loadSavedPreferences();

		getLoaderManager().initLoader(0, null, this);
		return mContactFragmentView;
	}

	public void loadSavedPreferences() {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		shirtCode = sp.getInt("COLOR", 0);
		GroupBitmap groupBMResult = new GroupBitmap();
		groupBMResult = updateHSV(imgSource, groupBMResult);
		imgView.setImageBitmap(groupBMResult.bitmapDest);
	}

	public static void refreshFragment(int shirtC) {
		shirtCode = shirtC;
		GroupBitmap groupBMResult = new Front().new GroupBitmap();
		groupBMResult = updateHSV(imgSource, groupBMResult);
		imgView.setImageBitmap(groupBMResult.bitmapDest);
	}

	public static void saveLayout() {		
		AppSingleton.setFront(null);
		
		viewSetting.setVisibility(View.GONE);
		textViewSetting.setVisibility(View.GONE);
		imgViewSetting.setVisibility(View.GONE);
		masterView.setVisibility(View.GONE);

		fragmentLayout.getRootView();
		fragmentLayout.setDrawingCacheEnabled(true);
		
		Bitmap bitmap = fragmentLayout.getDrawingCache();
		bitmap = Bitmap.createBitmap(bitmap, 0, 200, bitmap.getWidth(), bitmap.getHeight()-400);
		AppSingleton.setFront(bitmap);

		masterView.setVisibility(View.VISIBLE);
	}

	private void initializeVariables() {
		fragmentLayout = (RelativeLayout) mContactFragmentView
				.findViewById(R.id.fragment_container);
		masterView = (RelativeLayout) mContactFragmentView
				.findViewById(R.id.masterViewSetting);

		viewSetting = (RelativeLayout) mContactFragmentView
				.findViewById(R.id.viewSetting);
		textViewSetting = (RelativeLayout) mContactFragmentView
				.findViewById(R.id.textViewSetting);
		imgViewSetting = (RelativeLayout) mContactFragmentView
				.findViewById(R.id.imgViewSetting);
		// refer from:
		// http://developer.android.com/guide/topics/graphics/2d-graphics.html
		// https://www.youtube.com/watch?v=h7ablbgYOio
		imgView = (SvgImageView) mContactFragmentView
				.findViewById(R.id.shirtImgView);

		txtBtn = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonTxt);
		imgBtn = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonImg);
		circleBtn = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonCircle);
		squareBtn = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonSquare);

		txtBtn.setOnClickListener(txtBtnClickListener);
		imgBtn.setOnClickListener(imgBtnClickListener);
		circleBtn.setOnClickListener(circleBtnClickListener);
		squareBtn.setOnClickListener(squareBtnClickListener);

		colorPick = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonColor);
		add = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonAdd);
		minus = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonMinus);
		del = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonDelete);
		close = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonClose);

		colorPick.setOnClickListener(colorPickClickListener);
		add.setOnTouchListener(addScaleClickListener);
		minus.setOnTouchListener(minusScaleClickListener);
		del.setOnClickListener(delViewClickListener);
		close.setOnClickListener(closeViewSettingClickListener);

		colorPickTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonColorText);
		addTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonAddText);
		minusTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonMinusText);
		editTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonEditText);
		delTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonDeleteText);
		closeTxt = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonCloseText);

		colorPickTxt.setOnClickListener(colorPickClickListener);
		addTxt.setOnTouchListener(addScaleClickListener);
		minusTxt.setOnTouchListener(minusScaleClickListener);
		editTxt.setOnClickListener(editTextClickListener);
		delTxt.setOnClickListener(delViewClickListener);
		closeTxt.setOnClickListener(closeViewSettingClickListener);

		delImg = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonDeleteImg);
		closeImg = (ImageButton) mContactFragmentView
				.findViewById(R.id.imageButtonCloseImg);

		delImg.setOnClickListener(delViewClickListener);
		closeImg.setOnClickListener(closeViewSettingClickListener);

	}

	private void setupViews() {
		DragController dragController = mDragController;

		mDragLayer = (DragLayer) mContactFragmentView
				.findViewById(R.id.drag_layer);
		mDragLayer.setDragController(dragController);
		dragController.addDropTarget(mDragLayer);
	}

	private OnClickListener colorPickClickListener = new OnClickListener() {
		public void onClick(View v) {
			showColorPickerDialog();
		}
	};

	private OnTouchListener addScaleClickListener = (new RepeatListener(400,
			100, new OnClickListener() {
				@Override
				public void onClick(View view) {
					if (selectedItem instanceof TextView) {
						TextView selectedText = (TextView) selectedItem;
						float txtSize = selectedText.getTextSize();
						Log.i(AppSingleton.getProjectPackage(), "txtSize"
								+ txtSize);

						if (txtSize >= 100) {
							Toast.makeText(getActivity(),
									"Reach maximum text size",
									Toast.LENGTH_SHORT).show();
						} else {
							double newTxtSize = (txtSize + 1) / 2;
							Log.i(AppSingleton.getProjectPackage(),
									"newtxtSize" + newTxtSize);
							selectedText.setTextSize((float) newTxtSize);
						}
					} else {
						int height = selectedItem.getHeight();
						int width = selectedItem.getWidth();

						if (height >= 250 || width >= 250) {
							Toast.makeText(getActivity(),
									"Reach maximum scaling", Toast.LENGTH_SHORT)
									.show();
						} else {
							LayoutParams params = selectedItem
									.getLayoutParams();
							params.height = height + 5;
							params.width = width + 5;
							selectedItem.setLayoutParams(params);
						}
					}
				}
			}));

	private OnTouchListener minusScaleClickListener = (new RepeatListener(400,
			100, new OnClickListener() {
				@Override
				public void onClick(View view) {
					if (selectedItem instanceof TextView) {
						TextView selectedText = (TextView) selectedItem;
						float txtSize = selectedText.getTextSize();
						Log.i(AppSingleton.getProjectPackage(), "txtSize"
								+ txtSize);

						if (txtSize <= 20) {
							Toast.makeText(getActivity(),
									"Reach minimum text size",
									Toast.LENGTH_SHORT).show();
						} else {
							double newTxtSize = (txtSize - 1) / 2;
							Log.i(AppSingleton.getProjectPackage(),
									"newtxtSize" + newTxtSize);
							selectedText.setTextSize((float) newTxtSize);
						}
					} else {
						int height = selectedItem.getHeight();
						int width = selectedItem.getWidth();

						if (height <= 30 || width <= 30) {
							Toast.makeText(getActivity(),
									"Reach minimum scaling", Toast.LENGTH_SHORT)
									.show();
						} else {
							LayoutParams params = selectedItem
									.getLayoutParams();
							params.height = height - 5;
							params.width = width - 5;
							selectedItem.setLayoutParams(params);
						}
					}
				}
			}));

	private OnClickListener editTextClickListener = new OnClickListener() {
		public void onClick(View v) {
			final TextView selectedText = (TextView) selectedItem;

			// get alert dialog layout
			LayoutInflater li = LayoutInflater.from(context);
			View promptsView = li.inflate(R.layout.text_dialog, null);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			alertDialogBuilder.setView(promptsView);

			final EditText userInput = (EditText) promptsView
					.findViewById(R.id.editTextDialogUserInput);
			userInput.setText(selectedText.getText());

			// set dialog message
			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									if (userInput.getText().toString().trim()
											.equals("")) {
										Toast.makeText(getActivity(),
												"Empty text is not allowed",
												Toast.LENGTH_SHORT).show();
										// override on click listener
										addTextToScreen();
									} else {
										selectedText.setText(userInput
												.getText().toString());
										selectedItem
												.setLayoutParams(selectedText
														.getLayoutParams());
										dialog.cancel();
									}
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();

		}
	};

	private OnClickListener delViewClickListener = new OnClickListener() {
		public void onClick(View v) {
			if (selectedItem instanceof TextView) {
				textViewSetting.setVisibility(View.GONE);
			} else if (selectedItem instanceof ImageView) {
				imgViewSetting.setVisibility(View.GONE);
			} else {
				viewSetting.setVisibility(View.GONE);
			}
			ViewGroup parent = (ViewGroup) selectedItem.getParent();
			if (parent != null) {
				parent.removeView(selectedItem);
				selectedItem = null;
			}
		}
	};

	private OnClickListener closeViewSettingClickListener = new OnClickListener() {
		public void onClick(View v) {
			if (selectedItem instanceof TextView) {
				textViewSetting.setVisibility(View.GONE);
			} else if (selectedItem instanceof ImageView) {
				imgViewSetting.setVisibility(View.GONE);
			} else {
				viewSetting.setVisibility(View.GONE);
			}
			selectedItem = null;
		}
	};

	private OnClickListener txtBtnClickListener = new OnClickListener() {
		public void onClick(View v) {
			addTextToScreen();
		}
	};

	private OnClickListener imgBtnClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
		}
	};

	private OnClickListener circleBtnClickListener = new OnClickListener() {
		public void onClick(View v) {
			addShapeToScreen(R.drawable.circle);
		}
	};

	private OnClickListener squareBtnClickListener = new OnClickListener() {
		public void onClick(View v) {
			addShapeToScreen(R.drawable.rectangle);
		}
	};

	private void showColorPickerDialog() {

		ColorPickerDialog colorPickerDialog = new ColorPickerDialog(
				getActivity(), Color.WHITE, new OnColorSelectedListener() {

					@Override
					public void onColorSelected(int color) {
						String hexColor = String.format("#%02x%02x%02x",
								Color.red(color), Color.green(color),
								Color.blue(color));
						Log.i(AppSingleton.getProjectPackage(), "new color: "
								+ hexColor);

						if (selectedItem instanceof TextView) {
							TextView selectedText = (TextView) selectedItem;
							selectedText.setTextColor(Color
									.parseColor(hexColor));
						} else {
							GradientDrawable background = (GradientDrawable) selectedItem
									.getBackground();
							background.setColor(Color.parseColor(hexColor));
						}
					}

				});
		colorPickerDialog.show();
	}

	public void addTextToScreen() {
		final TextView newText = new TextView(mContactFragmentView.getContext());
		newText.setOnLongClickListener(this);
		newText.setOnClickListener(this);
		newText.setOnTouchListener(this);

		// get alert dialog layout
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.text_dialog, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInput);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (userInput.getText().toString().trim().equals("")) {
							Toast.makeText(getActivity(),
									"Empty text is not allowed",
									Toast.LENGTH_SHORT).show();
							// override on click listener
							addTextToScreen();
						} else {
							newText.setText(userInput.getText().toString());
							dialog.cancel();
							newText.setLayoutParams(new LayoutParams(
									LayoutParams.WRAP_CONTENT,
									LayoutParams.WRAP_CONTENT));
							mDragLayer.addView(newText);
						}
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();

	}

	public void addShapeToScreen(int res) {
		// adapter get fragment
		// fragment.addshapetoscreen;
		try {
			View newShape = new View(context);
			newShape.setLayoutParams(new LayoutParams(100, 100));
			newShape.setBackgroundResource(res);

			System.out.println(this.getClass().getSimpleName());

			mDragLayer.addView(newShape);

			newShape.setOnClickListener(this);
			newShape.setOnLongClickListener(this);
			newShape.setOnTouchListener(this);

		} catch (Exception e) {
			Log.i(AppSingleton.getProjectPackage(), "error adding shape: " + e);
		}
	}

	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return null;
	}

	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		mAdapter.swapCursor(arg1);
	}

	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE
				&& resultCode == getActivity().RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();

			ImageView imgView = new ImageView(context);
			imgView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
			imgView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));

			mDragLayer.addView(imgView);

			imgView.setOnClickListener(this);
			imgView.setOnLongClickListener(this);
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}

	@Override
	public void onClick(View v) {
		if (mLongClickStartsDrag) {
			textViewSetting.setVisibility(View.GONE);
			viewSetting.setVisibility(View.GONE);

			// store the state of selected view object
			selectedItem = v;
			// show the view options
			if (selectedItem instanceof TextView) {
				textViewSetting.setVisibility(View.VISIBLE);
			} else if (selectedItem instanceof ImageView) {
				imgViewSetting.setVisibility(View.VISIBLE);
			} else {
				viewSetting.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public boolean onLongClick(View v) {
		// start drag
		textViewSetting.setVisibility(View.GONE);
		imgViewSetting.setVisibility(View.GONE);
		viewSetting.setVisibility(View.GONE);

		selectedItem = v;
		if (selectedItem instanceof TextView) {
			textViewSetting.setVisibility(View.VISIBLE);
		} else if (selectedItem instanceof ImageView) {
			imgViewSetting.setVisibility(View.VISIBLE);
		} else {
			viewSetting.setVisibility(View.VISIBLE);
		}
		Object dragInfo = v;
		mDragController.startDrag(v, mDragLayer, dragInfo,
				DragController.DRAG_ACTION_MOVE);
		return true;
	}

	/********* Change shirt color *********/

	public class GroupBitmap {
		Bitmap bitmapDest;
	};

	private static int getHue() {
		int hue = 0;

		switch (shirtCode) {
		// white
		case 0:
			hue = 0;
			break;
		// black
		case 1:
			hue = 0;
			break;
		// red
		case 2:
			hue = 0;
			break;
		// green
		case 3:
			hue = 68;
			break;
		// blue
		case 4:
			hue = 162;
			break;
		// purple
		case 5:
			hue = 190;
			break;
		}

		return hue;
	}

	private static int getSat() {
		int sat = 0;

		switch (shirtCode) {
		// white
		case 0:
			sat = 0;
			break;
		// black
		case 1:
			sat = -256;
			break;
		// red
		case 2:
			sat = 231;
			break;
		// green
		case 3:
			sat = 230;
			break;
		// blue
		case 4:
			sat = 174;
			// purple
		case 5:
			sat = 152;
			break;
		}

		return sat;
	}

	private static int getVal() {
		int val = 0;

		switch (shirtCode) {
		// white
		case 0:
			val = 0;
			break;
		// black
		case 1:
			val = -174;
			break;
		// red
		case 2:
			val = 0;
			break;
		// green
		case 3:
			val = 0;
			break;
		// blue
		case 4:
			val = -68;
			// purple
		case 5:
			val = 0;
			break;
		}

		return val;
	}

	// Convert Bitmap from Color to HSV, then HSV to Color

	private GroupBitmap convertColorHSVColor(Bitmap src) {

		GroupBitmap convertedGroupBitmap = new GroupBitmap();

		int w = src.getWidth();
		int h = src.getHeight();

		int[] mapSrcColor = new int[w * h];
		int[] mapDestColor = new int[w * h];

		int[] mapHue = new int[w * h];
		int[] mapSat = new int[w * h];
		int[] mapVal = new int[w * h];

		float[] pixelHSV = new float[3];

		src.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

		int index = 0;
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {

				// Convert from Color to HSV
				Color.colorToHSV(mapSrcColor[index], pixelHSV);

				/*
				 * Represent Hue, Saturation and Value in separated color of R,
				 * G, B.
				 */
				mapHue[index] = Color
						.rgb((int) (pixelHSV[0] * 255 / 360), 0, 0);
				mapSat[index] = Color.rgb(0, (int) (pixelHSV[1] * 255), 0);
				mapVal[index] = Color.rgb(0, 0, (int) (pixelHSV[2] * 255));

				// Convert back from HSV to Color
				mapDestColor[index] = Color.HSVToColor(pixelHSV);

				index++;
			}
		}

		Config destConfig = src.getConfig();
		/*
		 * If the bitmap's internal config is in one of the public formats,
		 * return that config, otherwise return null.
		 */

		if (destConfig == null) {
			destConfig = Config.RGB_565;
		}

		convertedGroupBitmap.bitmapDest = Bitmap.createBitmap(mapDestColor, w,
				h, destConfig);

		return convertedGroupBitmap;
	}

	// Update HSV according to selected color setting

	private static GroupBitmap updateHSV(Bitmap src,
			GroupBitmap convertedGroupBitmap) {

		int progressHue = getHue();
		int progressSat = getSat();
		int progressVal = getVal();

		float settingHue = (float) progressHue * 360 / 256;
		float settingSat = (float) progressSat / 256;
		float settingVal = (float) progressVal / 256;

		// GroupBitmap convertedGroupBitmap = new GroupBitmap();

		int w = src.getWidth();
		int h = src.getHeight();

		int[] mapSrcColor = new int[w * h];
		int[] mapDestColor = new int[w * h];

		int[] mapHue = new int[w * h];
		int[] mapSat = new int[w * h];
		int[] mapVal = new int[w * h];

		float[] pixelHSV = new float[3];

		src.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

		int index = 0;
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {

				// Convert from Color to HSV
				Color.colorToHSV(mapSrcColor[index], pixelHSV);

				// Adjust HSV
				pixelHSV[0] = pixelHSV[0] + settingHue;
				if (pixelHSV[0] < 0) {
					pixelHSV[0] = 0;
				} else if (pixelHSV[0] > 360) {
					pixelHSV[0] = 360;
				}

				pixelHSV[1] = pixelHSV[1] + settingSat;
				if (pixelHSV[1] < 0) {
					pixelHSV[1] = 0;
				} else if (pixelHSV[1] > 1) {
					pixelHSV[1] = 1;
				}

				pixelHSV[2] = pixelHSV[2] + settingVal;
				if (pixelHSV[2] < 0) {
					pixelHSV[2] = 0;
				} else if (pixelHSV[2] > 1) {
					pixelHSV[2] = 1;
				}

				/*
				 * Represent Hue, Saturation and Value in separated color of R,
				 * G, B.
				 */
				mapHue[index] = Color
						.rgb((int) (pixelHSV[0] * 255 / 360), 0, 0);
				mapSat[index] = Color.rgb(0, (int) (pixelHSV[1] * 255), 0);
				mapVal[index] = Color.rgb(0, 0, (int) (pixelHSV[2] * 255));

				// Convert back from HSV to Color
				mapDestColor[index] = Color.HSVToColor(pixelHSV);

				index++;
			}
		}

		Config destConfig = src.getConfig();
		/*
		 * If the bitmap's internal config is in one of the public formats,
		 * return that config, otherwise return null.
		 */

		if (destConfig == null) {
			destConfig = Config.RGB_565;
		}

		convertedGroupBitmap.bitmapDest = Bitmap.createBitmap(mapDestColor, w,
				h, destConfig);

		return convertedGroupBitmap;
	}

}
