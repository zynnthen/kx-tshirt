package com.mobile.kstshirt;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;

import com.mobile.kstshirt.adapter.AppSectionsPagerAdapter;
import com.mobile.kstshirt.checkout.AllLayout;
import com.mobile.kstshirt.fragment.Back;
import com.mobile.kstshirt.fragment.Front;
import com.mobile.kstshirt.fragment.Left;
import com.mobile.kstshirt.fragment.Right;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {

	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	MyViewPager mViewPager;
	final CharSequence sizingList[] = { "XS", "S", "M", "L", "XL" };
	final CharSequence colorList[] = { "White", "Black", "Red", "Green",
			"Blue", "Purple" };
	int shirtCode = 0;
	int currentPos = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the pager adapter
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(
				getSupportFragmentManager());
		mViewPager = (MyViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mViewPager.setPagingEnabled(false);

		// save the state of all fragment
		mViewPager
				.setOffscreenPageLimit(mAppSectionsPagerAdapter.getCount() - 1);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						// ViewPager listener
						actionBar.setSelectedNavigationItem(position);
					}
				});

		for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
			// set tab icon and listener
			Tab tab = actionBar.newTab();
			tab.setTabListener(this);
			switch (i) {
			case 0:
				tab.setIcon(R.drawable.tshirt_front);

				break;
			case 1:
				tab.setIcon(R.drawable.tshirt_back);
				break;
			case 2:
				tab.setIcon(R.drawable.tshirt_left);
				break;
			case 3:
				tab.setIcon(R.drawable.tshirt_right);
				break;
			default:
				tab.setIcon(R.drawable.ic_launcher);
				break;
			}
			actionBar.addTab(tab);
		}

	}

	@Override
	public void onBackPressed() {
		// exit application option
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		// set dialog message
		builder.setMessage("Exit application?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		// Show the AlertDialog
		builder.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		SubMenu sub = menu.addSubMenu("Submenu").setIcon(
				R.drawable.ic_ab_overflow);
		sub.add(0, R.id.optmenu_color_picker, 0, getApplicationContext()
				.getString(R.string.action_color_picker));
		sub.add(0, R.id.optmenu_info, 0,
				getApplicationContext().getString(R.string.action_info));
		sub.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.optmenu_check_out:
			saveLayout();
			Intent i = new Intent(this, AllLayout.class);
			startActivity(i);
			return true;
		case R.id.optmenu_color_picker:
			colorOptiondialog();
			return true;
		case R.id.optmenu_info:
			pricingInfoDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void saveLayout() {
		Front.saveLayout();
		Back.saveLayout();
		Left.saveLayout();
		Right.saveLayout();
	}

	private void pricingInfoDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		// set dialog message
		builder.setTitle("Pricing info").setMessage(R.string.pricing_info)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Show the AlertDialog
		builder.show();
	}

	private void colorOptiondialog() {

		loadSavedPreferences();

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		// set dialog message
		builder.setTitle("Select color").setSingleChoiceItems(colorList,
				shirtCode, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						// refer from:
						// http://stackoverflow.com/questions/26606527/android-refresh-a-fragment-list-from-its-parent-activity
						// getFragmentRefreshListener().onRefresh();

						// using shared preferences technique
						savePreferences("COLOR", which);
						dialog.cancel();
					}
				});

		// Show the AlertDialog
		builder.show();
	}

	private void loadSavedPreferences() {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		shirtCode = sp.getInt("COLOR", 0);
	}

	private void savePreferences(String key, int value) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor edit = sp.edit();
		edit.putInt(key, value);
		edit.commit();

		// update fragments
		for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
			switch (i) {
			case 0:
				Front.refreshFragment(value);
				break;
			case 1:
				Back.refreshFragment(value);
				break;
			case 2:
				Left.refreshFragment(value);
				break;
			case 3:
				Right.refreshFragment(value);
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
		currentPos = tab.getPosition();
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

}
