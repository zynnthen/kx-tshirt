package com.mobile.kstshirt;

import com.mobile.kstshirt.adapter.ArrayAdapterWithIcon;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;

public class StartUp extends Activity {

	final String [] items = new String[] {"T shirt", "V-neck", "Polo Shirt"};
	final Integer[] icons = new Integer[] { R.drawable.tshirt_front, R.drawable.vfront, R.drawable.pfront };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ListAdapter adapter = new ArrayAdapterWithIcon(this, items, icons);

		AlertDialog.Builder builder = new AlertDialog.Builder(StartUp.this);
		builder.setCancelable(false);
		builder.setTitle("Select shirt design");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				AppSingleton.setSelectedShirt(item);
				finish();
				Intent i = new Intent(StartUp.this, MainActivity.class);
				startActivity(i);
			}
		});
		builder.show();
	}
	
}
