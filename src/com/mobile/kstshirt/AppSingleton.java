package com.mobile.kstshirt;

import android.graphics.Bitmap;

public class AppSingleton {

	final static String ProjectPackage = "com.mobile.kstshirt";
	static int selectedShirt = 0;
	static Bitmap front, back, left, right = null;

	public static Bitmap getFront() {
		return front;
	}
	
	public static Bitmap getBack() {
		return back;
	}

	public static Bitmap getLeft() {
		return left;
	}

	public static Bitmap getRight() {
		return right;
	}
	
	public static void setFront(Bitmap front) {
		AppSingleton.front = front;
	}

	public static void setBack(Bitmap back) {
		AppSingleton.back = back;
	}

	public static void setLeft(Bitmap left) {
		AppSingleton.left = left;
	}

	public static void setRight(Bitmap right) {
		AppSingleton.right = right;
	}

	public static String getProjectPackage() {
		return ProjectPackage;
	}

	public static int getSelectedShirt() {
		return selectedShirt;
	}

	public static void setSelectedShirt(int selectedShirt) {
		AppSingleton.selectedShirt = selectedShirt;
	}
	
}
